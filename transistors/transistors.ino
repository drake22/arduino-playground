///////////////////////
//  LIBRARY FUNCTIONS
///////////////////////
int i_pow( int base, int exponent ) {
  int total = 1;
  for( int i = 0; i < exponent; i++ ) {
    total *= base;
  }
  return total;
}

//////////////
// CONSTANTS
//////////////
const int PIN_OUT_1 = 2;
const int PIN_OUT_2 = 3;
const int DELAY_MS = 1000;

/////////////
//  PROGRAM
/////////////
void setup() {
  pinMode( PIN_OUT_1, OUTPUT );
  digitalWrite( PIN_OUT_1, LOW );
  pinMode( PIN_OUT_2, OUTPUT );
  digitalWrite( PIN_OUT_2, LOW );
  Serial.begin( 9600 );
}

void loop() {
  Serial.println( "HIGH, HIGH" );
  digitalWrite( PIN_OUT_1, HIGH );
  digitalWrite( PIN_OUT_2, HIGH );
  delay( DELAY_MS );
  
  Serial.println( "LOW, HIGH" );
  digitalWrite( PIN_OUT_1, LOW );
  digitalWrite( PIN_OUT_2, HIGH );
  delay( DELAY_MS );
  
  Serial.println( "HIGH, LOW" );
  digitalWrite( PIN_OUT_1, HIGH );
  digitalWrite( PIN_OUT_2, LOW );
  delay( DELAY_MS );
  
  Serial.println( "LOW, LOW" );
  digitalWrite( PIN_OUT_1, LOW );
  digitalWrite( PIN_OUT_2, LOW );
  delay( DELAY_MS );
}
