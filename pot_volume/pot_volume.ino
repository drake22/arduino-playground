///////////////////////
//  LIBRARY FUNCTIONS
///////////////////////
int i_pow( int base, int exponent ) {
  int total = 1;
  for( int i = 0; i < exponent; i++ ) {
    total *= base;
  }
  return total;
}

//////////////
// CONSTANTS
//////////////
const int LED_COUNT = 8;
const int FIRST_PIN = 2;
const int MAX_OUTPUT_VALUE = i_pow( 2, LED_COUNT ) - 1;
const float MAX_ANALOG_VALUE = 1023.0f;
const byte ANALOG_PIN_RIGHT = 0;
// const byte ANALOG_PIN_LEFT = 1;
const int DELAY_MS = 10;
const boolean BIT_MASKS[] = { 1, 2, 4, 8, 16, 32, 64, 128 };

/////////////
//  PROGRAM
/////////////
void setup() {
  for( int i = 0; i < LED_COUNT; i++ ) {
    pinMode(i + FIRST_PIN, OUTPUT);
    digitalWrite(i + FIRST_PIN, LOW);
  }
  Serial.begin(9600);
}

void set_output( int output ) {
  for( int i = 0; i < LED_COUNT; i++ ) {
    int bit_high = BIT_MASKS[ i ] & output;
    
    if( bit_high ) {
      digitalWrite( i + FIRST_PIN, HIGH );
    } else {
      digitalWrite( i + FIRST_PIN, LOW );
    }
  }
}

void loop() {
  float analog_value = ( analogRead( ANALOG_PIN_RIGHT ) / MAX_ANALOG_VALUE ) * MAX_OUTPUT_VALUE;
  
  set_output( analog_value );
  
  // Serial.println( "LEFT" );
  // Serial.println( analogRead( ANALOG_PIN_LEFT ) );
  Serial.println( "RIGHT" );
  Serial.println( analogRead( (int) analog_value ) );
  
  delay( DELAY_MS );
}
