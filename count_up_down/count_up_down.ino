///////////////////////
//  LIBRARY FUNCTIONS
///////////////////////
int i_pow( int base, int exponent ) {
  int total = 1;
  for( int i = 0; i < exponent; i++ ) {
    total *= base;
  }
  return total;
}

//////////////
// CONSTANTS
//////////////
const int LED_COUNT = 8;
const int DELAY_MS = 1000;
const int MAX_VALUE = i_pow( 2, LED_COUNT ) - 1;
const boolean BIT_MASKS[] = { 1, 2, 4, 8, 16, 32, 64, 128 };

/////////////
//  PROGRAM
/////////////
void set_output( int output ) {
  for( int i = 0; i < LED_COUNT; i++ ) {
    int bit_high = BIT_MASKS[ i ] & output;
    
    if( bit_high ) {
      digitalWrite( i, HIGH );
    } else {
      digitalWrite( i, LOW );
    }
  }
}

void setup() {
  for( int i = 0; i < LED_COUNT; i++ ) {
    pinMode(i, OUTPUT);
    digitalWrite(i, LOW);
  }  
}

void loop() {
  static byte output = 0;
  static boolean count_up = true;
  
  set_output( output );
  
  if( output == 0 ) {
    count_up = true; 
  } else if( output == MAX_VALUE ) {
    count_up = false;
  }
  
  if( count_up ) {
    output++;
  } else {
    output--;
  }
  
  delay( DELAY_MS );
}
